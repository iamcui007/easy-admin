package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApTest;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApTestRequest;
import java.util.List;

/**
 * 测试接口
 *
 * @author mars
 * @date 2023-12-09
 */
public interface IApTestService {
    /**
     * 新增
     *
     * @param param param
     * @return ApTest
     */
    ApTest add(ApTestRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApTestRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApTest
     */
    ApTest getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApTest>
     */
    PageInfo<ApTest> pageList(ApTestRequest param);

}
