package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import java.util.*;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.time.LocalDateTime;
import com.mars.module.system.entity.BaseEntity;

    /**
 * 测试对象 ap_test
 *
 * @author mars
 * @date 2023-12-09
 */

@Data
@ApiModel(value = "测试对象")
@Builder
@Accessors(chain = true)
@TableName("ap_test")
public class ApTest extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 图片
     */
    @Excel(name = "图片")
    @ApiModelProperty(value = "图片")
    private String picture;

    /**
     * 状态
     */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private Integer state;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间" , width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人名称
     */
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 更新人名称
     */
    @ApiModelProperty(value = "更新人名称")
    private String updateByName;
}
