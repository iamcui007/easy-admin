package ${packageName}.entity;

#foreach ($import in $importList)
import ${import};
#end
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import java.util.*;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.time.LocalDateTime;
#if(${genBaseField})
import com.mars.module.system.entity.BaseEntity;
#end

    /**
 * ${functionName}对象 ${tableName}
 *
 * @author ${author}
 * @date ${datetime}
 */
#*#if($table.crud)
#set($Entity="BaseEntity")
#elseif($table.tree)
#set($Entity="TreeEntity")
#end*#
@Data
@ApiModel(value = "${functionName}对象")
@Builder
@Accessors(chain = true)
@TableName("${tableName}")
#if(${genBaseField})
public class ${ClassName} extends BaseEntity {
#else
public class ${ClassName}{
#end

#foreach ($column in $columns)

    /**
     * $column.columnComment
     */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($parentheseIndex != -1)
    @Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
#elseif($column.javaField == 'createTime')
    @Excel(name = "${comment}" , width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT, value = "create_time")
#elseif($column.javaField == 'updateTime')
    @Excel(name = "${comment}" , width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.UPDATE, value = "update_time")
#elseif($column.javaField == 'createById')
    @TableField(fill = FieldFill.INSERT, value = "create_by_id")
#elseif($column.javaField == 'createByName')
    @TableField(fill = FieldFill.INSERT, value = "create_by_name")
#elseif($column.javaField == 'updateById')
    @TableField(fill = FieldFill.UPDATE, value = "update_by_id")
#elseif($column.javaField == 'updateByName')
    @TableField(fill = FieldFill.UPDATE, value = "create_by_name")
#else
    @Excel(name = "${comment}")
#end
#if($column.javaType == 'Long')
    @JsonFormat(shape = JsonFormat.Shape.STRING)
#end
#end
#if($column.isPk==1)
    @TableId(value = "$column.columnName", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "$column.columnName")
    private $column.javaType $column.javaField;
#else
    @ApiModelProperty(value = "$column.columnComment")
    private $column.javaType $column.javaField;
#end
#end
}
