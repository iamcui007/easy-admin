/**
 * 分页查询战队列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/tTeam/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询战队详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/tTeam/query/' + id,
        method: 'get'
    })
}

/**
 * 新增战队
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/tTeam/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改战队
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/tTeam/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除战队
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/tTeam/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出战队
 *
 * @param query
 * @returns {*}
 */
function exportTeam(query) {
    return requests({
        url: '/admin/tTeam/export',
        method: 'get',
        params: query
    })
}
